.. index::
   ! Judaïsme


.. raw:: html

   <a rel="me" href="https://babka.social/@pvergain"></a>
   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://kolektiva.social/@noamsw"></a>


|FluxWeb| `RSS <https://judaism.gitlab.io/judaisme/rss.xml>`_

.. _judaisme:

=======================================================================================================
🕎 🕯️🌄🎶💐💗⚖  **Judaïsme** 🕎🕯️ 🌄🎶💐💗⚖
=======================================================================================================

- https://fr.wikipedia.org/wiki/Portail:Juda%C3%AFsme
- https://www.jewfaq.org/index.shtml


.. toctree::
   :maxdepth: 6

   actions/actions
   fetes/fetes


.. toctree::
   :maxdepth: 4


   calendriers/calendriers
   humour/humour
   people/people
   livres/livres
   lieux/lieux
   ressources/ressources
   glossaire/glossaire
   news/news


.. index::
   ! Ressources

.. _ressources:

=======================================================================================================
Ressources
=======================================================================================================

- :ref:`link_judaisme:linkertree_judaisme`


- http://linkertree.frama.io/judaisme


#ShalomSalaam #CeaseFireNow #Israel #Gaza #Mazeldon #Jewdiverse #Peace #Palestine
#jewish #ethics #morals #values #betterworld #israel #hebrew

Mastowall
===========

- https://rstockm.github.io/mastowall/?hashtags=mazeldon,jewish,jewdiverse&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=torah&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=chinsky&server=https://framapiaf.org


Calendriers
============

- http://www.calj.net/
- https://www.hebcal.com/

https://rabbinchinsky.fr
===========================

- https://rabbinchinsky.fr


Youtube
=======

- https://www.youtube.com/c/FlorianeChinsky/videos
- https://invidious.fdn.fr/channel/UCDtvtoXqGz0Niwl2QGM8gyQ

Peertube
=========

- https://peertube.iriseden.eu/a/floriane_c/video-channels


Bokertov
===========

- https://judaism.gitlab.io/bokertov/


Judaisme
==========

- https://judaism.gitlab.io/judaisme/


Tenoua
==========

- https://www.tenoua.org/
- https://nitter.moomoo.me/Tenoua/rss

Judaisme en mouvement
=======================

- https://judaismeenmouvement.org/


Grenoble
===========

.. toctree::
   :maxdepth: 3

   grenoble/grenoble

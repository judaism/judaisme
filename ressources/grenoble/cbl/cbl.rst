
.. index::
   pair: Grenoble;  Centre Bernard Lazare (CBL)

.. _cbl_grenoble:

=======================================================================================================
Centre Bernard Lazare Grenoble
=======================================================================================================

- https://www.cbl-grenoble.org/

.. figure:: cblg_logo.png
   :align: center

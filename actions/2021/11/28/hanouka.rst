
.. _hanouka_2021_11_28:

=====================================================================================================
Hanouka le dimanche 28 novembre 2021 (24 Kislev 5782) à 16h dans la salle Olympe de Gouges 🕯
=====================================================================================================

- https://judaismeenmouvement.org/actualites/hanouka-5782/

Texte
========


Chers amis,

C'est une grande première depuis la naissance de Judaïsme En Mouvement,
cette année, nous organisons une `grande fête pour Hanouka <https://judaismeenmouvement.org/agenda/grande-fete-de-hanouka/>`_ avec le
soutien du `KKL <https://www.kkl.fr/>`_ (https://www.kkl.fr/) et tous les enfants du Talmud Torah de JEM.

Venez tous fêter Hanouka le dimanche 28 novembre à 16h dans la salle
`Olympe de Gouges <https://fr.wikipedia.org/wiki/Olympe_de_Gouges>`_ (`15 rue Merlin - 75011 Paris <https://www.geoportail.gouv.fr/carte?c=2.3855696444833434,48.86011743133446&z=18&l0=GEOGRAPHICALGRIDSYSTEMS.MAPS.3D::GEOPORTAIL:OGC:WMTS==aggregate(1)&l1=LIMITES_ADMINISTRATIVES_EXPRESS.LATEST::GEOPORTAIL:OGC:WMTS(1)&l2=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2::GEOPORTAIL:OGC:WMTS(1)&permalink=yes>`_)


Au programme
==============

- Allumage des bougies 🕯 avec les Rabbins :ref:`Floriane Chinsky <floriane:floriane_chinsky>` et
  Gabriel Farhi,
- grande chorale des enfants du Talmud Torah, animations,
  beignets, ballons, musique et **beaucoup de joie**.

Que les lumières de Hanouka illuminent vos vies et vos familles !

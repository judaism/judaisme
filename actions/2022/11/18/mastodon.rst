
.. _mazeldon_2022_11_18:

=============================================================================================
2022-11-18 mastodon #mazeldon
=============================================================================================


https://kibitz.cloud/@BenYehudaPress/109365744528816812, Some of our authors on Mastodon for your #FollowFriday needs
=========================================================================================================================

Some of our authors on Mastodon for your #FollowFriday needs / desires:

- https://babka.social/@projecttzimtzum
- https://wandering.shop/@rblemberg
- https://wandering.shop/@GillianPolack
- https://wandering.shop/@LadyIsak
- https://masto.ai/@abbystein
- https://zirk.us/@drzackaryberger
- https://wandering.shop/@mattkressel
- https://wandering.shop/@Allan_author_SF
- https://universeodon.com/@OnSophiaStreet
- https://journa.host/@Yudel


Let us know if you'd like to be added, we probably missed a lot of you!

#WritingCommunity #publishing


https://fenetre.dev/@rochelle/109365711165197392, For #FollowFriday, I'm recommending GROUPS!
===================================================================================================

- https://fenetre.dev/@rochelle/109365711165197392

❓ How do groups work ?
-------------------------

Follow the group to receive the posts.

Mention the group's @ when writing a post to have your post shared with
the group.

Groups I'm in:

- https://a.gup.pe/u/yoga
- https://a.gup.pe/u/mazeldon
- https://a.gup.pe/u/plants
- https://a.gup.pe/u/3goodthings

Lots more groups here, including directions on how to form your own
group: https://a.gup.pe/


.. index::
   ! Une tache aveugle

.. _une_tache_aveugle:

=======================================================================================================
**Les Juifs, une tache aveugle dans le récit national**
=======================================================================================================

- https://www.albin-michel.fr/les-juifs-une-tache-aveugle-dans-le-recit-national-9782226459824


.. figure:: une_tache_aveugle.jpg
   :align: center

Description
=============

Présents dès l’Empire romain sur le territoire de la France actuelle,
les juifs sont le plus souvent relégués **dans un angle mort de l’historiographie**,
et cette **tache aveugle** dans le récit national est particulièrement
manifeste dans les manuels scolaires, de la IIIe République à nos jours.

Pourquoi les expulsions médiévales ne sont-elles jamais mentionnées à
partir de l’époque moderne ?

Et, lorsqu’on évoque les juifs dans l’histoire de France, pourquoi est-ce
le plus souvent sous l’angle des persécutions qu’ils eurent à subir **et
non de l’originalité de leurs contributions ?**

- En quoi l’écriture actuelle de cette histoire est-elle encore tributaire
  de modèles archaïques ?
- Comment l’aborder dans l’enseignement secondaire et universitaire ?
- Quelles perspectives l’archéologie ouvre-t-elle ?
- Quel rôle les musées peuvent-ils jouer ?

Archéologues, historiens, sociologues, conservateurs et enseignants réunis
au musée d’art et d’histoire du Judaïsme en 2019 éclairent ces questions
qui renvoient également à la place des minorités dans la nation.




.. _bio_walter_benjamin:

=======================================================================================================
Biographie de Walter Benjamin
=======================================================================================================

- https://fr.wikipedia.org/wiki/Walter_Benjamin


Introduction
==============

Walter Benjamin [ˈvaltɐ ˈbɛnjamiːn] est un philosophe, historien de l'art,
critique littéraire, critique d'art et traducteur allemand, né le
15 juillet 1892 à Berlin (Allemagne) et mort le 26 septembre 1940 à
Portbou (Catalogne, Espagne).

Il est rattaché à l'école de Francfort. Il a notamment traduit Balzac,
Baudelaire et Proust, et est l'auteur d'une œuvre hétéroclite et
interdisciplinaire, aux confluents de la littérature, de la philosophie
et des sciences sociales.

Son suicide tragique a frappé du sceau de l'inachèvement un corpus déjà
extrêmement fragmentaire dans sa forme, composé de seulement deux livres
publiés de son vivant, d'articles et de nombreuses notes préparatoires
pour le grand projet de sa vie, qui ne verra jamais le jour : une vaste
enquête sur le Paris du XIXe siècle.

Sa pensée a largement été redécouverte, explorée et commentée à partir
des années 1950, avec la publication de nombreux textes inédits et de
sa correspondance.

Après être resté longtemps connu seulement dans les cercles littéraires,
il a peu à peu conquis une notoriété exceptionnelle, jusqu'à être aujourd'hui
considéré comme un des théoriciens les plus importants du XXe siècle.


Enfance et jeunesse
=======================

**Walter Bendix Schönflies Benjamin** naît à Berlin-Charlottenburg de
parents allemands de confession juive et assimilés, Émile Benjamin (1856-1926)
et Pauline (née Schoenflies).

Son père était d'abord banquier à Paris, puis antiquaire et marchand
d'art à Berlin : son fils héritera de son goût pour la collection.

Aîné de la fratrie, Walter a également un frère, Georg (1895–1942) et
une sœur, Dora (1901–1946).

Par ailleurs, Benjamin est le neveu du psychologue William Stern, ainsi
que le cousin de la poétesse Gertrud Kolmar (par sa mère) et du
**philosophe et activiste Günther Anders, époux d'Hannah Arendt**.



.. _remise_prix_walter_benjamin_2021:

=======================================================================================================
Remise des Prix Walter Benjamin 2021
=======================================================================================================

- https://www.youtube.com/watch?v=1jIPrgRbj_o
- https://fediverse.org/RaouxNathalie/status/1461299204681310209?s=20


http://www.editionscommune.org/article-actualite-73084698.html
================================================================

- http://www.editionscommune.org/article-actualite-73084698.html


Prix Walter Benjamin C'est notre livre édité en 2016, Mais de quoi ont-ils eu si peur ?
de Christine Breton et Sylvain Maestraggi, qui a reçu le prix Walter Benjamin 2021
« Mobiliser le savoir en direction du public et le public en direction du savoir »,
selon les propres mots de Walter Benjamin, tel est l'engagement de
l’Association Prix Walter Benjamin (APWB), qui déploie son activité depuis
ces terres catalanes où l’intellectuel allemand décida de mettre fin à
ses jours, en septembre 1940, pour échapper au nazisme et à ses complices
français.

Le jury a choisi cette année de récompenser une œuvre originale et novatrice
tant dans son fond que dans sa forme et qui contribue à faire connaître
la pensée et la vie de Walter Benjamin.

Les éditions commune tiennent à remercier chaleureusement l'association,
les membres du Jury et sa présidente Nathalie Raoux pour ces deux jours
de rencontres et de contributions passionnantes !

Un grand merci également à nos hôtes à Céret et à Carcassonne.


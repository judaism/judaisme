.. index::
   pair: 2021; Prix Walter Benjamin

.. _prix_walter_benjamin_2021:

=======================================================================================================
Prix Walter Benjamin 2021
=======================================================================================================

- https://www.youtube.com/watch?v=1jIPrgRbj_o
- https://fediverse.org/RaouxNathalie/status/1461299204681310209?s=20

.. toctree::
   :maxdepth: 3

   entretien/entretien
   remise_des_prix/remise_des_prix

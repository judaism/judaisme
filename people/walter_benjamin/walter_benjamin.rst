
.. index::
   ! Walter Benjamin

.. _walter_benjamin:

=======================================================================================================
Walter Benjamin (1892-07-15 -> 1940-09-26)
=======================================================================================================

- https://fr.wikipedia.org/wiki/Walter_Benjamin


.. toctree::
   :maxdepth: 3

   biographie/biographie
   prix_walter_benjamin/prix_walter_benjamin

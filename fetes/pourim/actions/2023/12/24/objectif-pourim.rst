.. index::
   pair: Pourim; Objectif Pourim !  Nous avons trois mois pour nous préparer à la fête de l’humour juif !* (2023-12-24)

.. _pourim_2023_12_24:

=====================================================================================================================================================
2023-12-24 **Objectif Pourim !  Nous avons trois mois pour nous préparer à la fête de l’humour juif !**
=====================================================================================================================================================

- https://rabbinchinsky.fr/2023/12/24/objectif-pourim/
- https://devenirjuif.wordpress.com/2023/12/18/que-savez-vous-de-pourim/
- https://rabbinchinsky.fr/2017/02/23/chants-meguila-pourim
- https://rabbinchinsky.fr/2017/02/26/bible-pedagogie-esther


Nous avons trois mois pour nous préparer à la fête de l’humour juif !

Date de la fête
====================

Samedi 23 mars à 18h et Dimanche matin à 10h30.

Date du cours de préparation: Samedi 27 janvier 2024 de 13h à 14h30.

Au programme
=================

- 13h déjeuner chabbatique et intervention rabbinique:
  "Le sens spirituel de Pourim et ses pratiques"

- 13h45 atelier "Mon pourim familial et communautaire".

Pour vous préparer, voici quelques ressources:

- `QUIZZ sur Pourim <https://devenirjuif.wordpress.com/2023/12/18/que-savez-vous-de-pourim/>`_

- `Tout sur les chants de Pourim ICI <https://rabbinchinsky.fr/2017/02/23/chants-meguila-pourim>`_

- https://rabbinchinsky.fr/2017/02/26/bible-pedagogie-esther

- `Une vidéo a propos de l’idolâtrie et de Pourim ICI <https://youtu.be/WekmZEkKKME>`_

- `Des vidéos pour apprendre des versets de la méguila: les versets lus par toute la communauté ICI <https://youtu.be/yNqPxGwbFBI>`_,

- `les taamim de la méguila ICI <https://youtu.be/JrIMumIOOyw>`_,

- `les versets lus par les personnages de Pourim ICI <https://youtu.be/8GlIFTWLql4?list=PLnHlXjFx9rOT1_f0psAcrzsN99II_btUc>`_

- `Une lecture intégrale de la méguila en hébreu ICI <http://www.virtualcantor.com/purim.htm>`_

- `Pourim en hébreu ici <https://devenirjuif.wordpress.com/2023/12/18/quelques-mots-dhebreu-pour-preparer-pourim/>`_

.. youtube:: WekmZEkKKME



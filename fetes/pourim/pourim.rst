.. index::
   ! Pourim

.. _pourim:

=====================================================================================================================================================
**Pourim**
=====================================================================================================================================================

- https://fr.wikipedia.org/wiki/Pourim
- https://www.pourimshpilunesco.eu/
- https://fr.wikipedia.org/wiki/Xerx%C3%A8s_Ier
  (Il est assimilé à Assuérus, le roi perse cité dans la Bible, en particulier
  dans le Livre d'Esther, par certains historiens modernes.
- https://www.myjewishlearning.com/article/9-things-you-didnt-know-about-purim/

.. toctree::
   :maxdepth: 6

   actions/actions

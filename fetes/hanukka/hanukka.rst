.. index::
   ! Hanoucca
   ! Hanouka
   ! Chanukah
   ! Chanuka

.. https://emojis.wiki/fr/hanoucca/
.. _hanoucca:

=====================================================================================================================================================
🕎 **Hanoucca Fête des fête des** 🔥 **lumières**, 🕎)  🕯️🕎🔥
=====================================================================================================================================================

- https://www.myjewishlearning.com/article/women-as-role-models/


.. figure:: images/Hanouka.png
   :align: center

.. toctree::
   :maxdepth: 3

   definitions/definitions
   vocabulaire/vocabulaire
   temps/temps


.. _hanoucca_wikipedia:

=================================================================================================================================================
**Hanoucca, Hanouka, chanukah, chanuka**  (en hébreu: חג החנוכה Hag HaHanoukka, Fête de l'Édification ou "de l'Encénie", **Fête des lumières**)
=================================================================================================================================================

- https://fr.wikipedia.org/wiki/Hanoucca

Description
==============

Hanoucca (en hébreu : חג החנוכה Hag HaHanoukka, **Fête de l'Édification**
ou **de l'Encénie**) est une fête juive d'institution rabbinique, commémorant
la réinauguration de l'autel des offrandes dans le Second Temple de Jérusalem,
lors de son retour au culte judaïque, après trois ans d'interruption et
de fermeture par le roi Antiochus IV Épiphane des Séleucides.

Elle marque une **importante victoire militaire des Maccabées** et symbolise
la résistance spirituelle du judaïsme à l'assimilation hellénistique.

Selon la tradition rabbinique, au cours de cette consécration, se produisit
le miracle de la fiole d'huile, permettant aux prêtres du Temple de faire
brûler pendant huit jours une quantité d'huile à peine suffisante pour
une journée.
C'est pourquoi Hanoucca est aussi appelée la **Fête des Lumières**.

Elle est célébrée à partir du 25 kislev (qui correspond, selon les années,
aux mois de novembre ou décembre dans le calendrier grégorien) et dure
huit jours, jusqu'au 2 ou 3 tevet (en fonction de la longueur de kislev,
mois de 29 ou 30 jours).

Les pratiques et coutumes qui s'y rattachent sont liées au miracle de la
fiole d'huile, en particulier l'allumage du chandelier à neuf branches
de Hanoucca pendant les huit jours de la fête et la consommation de
friandises à base d'huile d'olive (latkes, soufganiyot…).

On y joue aussi avec des toupies à quatre faces.



.. _hanoucca_emoji:

=================================================================================================================================================
🕎 Hanoucca Collection Emoji
=================================================================================================================================================

- https://emojis.wiki/fr/hanoucca/


Description
============

Hanoucca est une 🎉 Fête juive symbolisant le triomphe de la lumière sur
les ténèbres, dont la célébration commence le 25 Kislev (le troisième mois
du calendrier juif, correspond à 🍂 Novembre ou 🎄 Décembre) et dure huit
jours jusqu’au deuxième ou troisième Tevet (le quatrième mois du calendrier
juif, tombe en décembre ou ❄️ Janvier).


Faits intéressants
----------------------

- L’émoji représente la 🕎 Menorah avec 9 bougies allumées et est utilisé
  pour Hanoucca. Il a été approuvé par Unicode et ajouté à la version 1.0
  des émoji en 2015.
- La date de Hanoucca est variable, donc chaque année elle change en
  fonction du 📅 Calendrier juif. En 2021, elle durera du 28 novembre
  au soir jusqu’au 6 décembre au soir.


1. Histoire de la fête de 🕎 Hanoucca
==========================================

En 170 avant JC. Antiochus IV Épiphane, le 🤴 Roi syrien de la dynastie
séleucide, s’empare du 🕍 Temple de Jérusalem. Après cela, il envoya en
Syrie tous les biens sacrés du temple, y compris la 🕎 Ménorah en or.

En raison de la profanation du lieu saint d’Israël par Antiochus Épiphane,
le service dans le temple a été arrêté pendant 3 ans.

A la même époque, le peuple juif se divise en juifs orthodoxes, ceux qui
restent fidèles aux valeurs de la Torah, et en juifs hellénisés, ceux
qui pratiquent des croyances païennes.

En 167 avant JC. commence la lutte des Maccabées (Hasmonéens) contre les
Séleucides.

Le chef des Hasmonéens, le prêtre Matityahu Hashmonai, proclame : « Que
celui qui est pour Dieu me suive ! » Le peuple qui répondit le suivit et
mena une longue guérilla. Après sa mort, son fils, Yehuda Maccabee, se
mit à la tête de l’armée.

En 164 avant JC. Jérusalem et le Temple ont été libérés.
Les Maccabées ont libéré le temple de Jérusalem et ont rétabli la tradition
d’allumer la Ménorah le 25 Kislev.

Fait intéressant: Il existe plusieurs origines différentes pour le nom
de Hanoucca.
La première hypothèse provient probablement de l’expression « hanukkat ha-mizbeah »,
qui signifie la consécration de l’autel.

Selon la seconde hypothèse, l’expression marque la phrase « חנוכה = חנו כ « ה »,
qui se traduit par « ils se sont reposés [des ennemis] le 25 [jour du mois de Kislev] ».

Aussi, certains interprètent le nom de cette fête comme Hag Urim, qui
est la fête des 🔥 Lumières.


Traditions et symboles de 🕎 Hanoucca
=====================================

La fête de Hanoucca dure 8 jours. Selon la légende, après la libération
du 🕍 Temple, l’armée maccabéenne ne trouva pas la quantité d’huile
nécessaire pour allumer la 🕎 Ménorah et consacrer le temple.

L’huile trouvée n’aurait duré qu’un jour, mais les Maccabées ont néanmoins
allumé la ménorah et un miracle s’est produit: il y avait assez d’huile
pour 8 jours.

D’où la tradition principale d’allumer des 🕯 Bougies tout au long de
Hanoucca.

Il y a un certain ordre:

- Le premier jour, vous devez allumer une bougie;
- Le deuxième, en allumer deux;
- Le troisième, en allumer trois;
- Et ainsi de suite jusqu’à huit bougies.

Dans ce cas, vous devez commencer à allumer les bougies à partir de celle
qui est la plus éloignée de la 🚪 Porte.

De même, un shamash, une bougie supplémentaire que l’on allume chaque
jour avant les bougies principales, est utilisé pour allumer toutes les
bougies.

Traditionnellement, à Hanoucca, 44 bougies sont utilisées, qui sont allumées
dans un chandelier spécial **Chanukiah**, qui est l’un des principaux symboles de la fête.

Traditionnellement, des plats de 🥛 Lait sont servis à Hanoucca, en
souvenir du salut d’Israël par Judith.

Il est également d’usage de manger des sufganiyot, qui sont des 🍩 Beignets
sucrés frits avec de la confiture, du lait concentré ou du 🍫 Chocolat
dans du sucre en poudre, et des levivot (crêpes de pommes de terre).

Dans la tradition ashkénaze, le jour de Hanoucca les 👦 Enfants reçoivent
de 💵 l’argent Hanoucca, des 🪙 Pièces de monnaie en chocolat et sont
aussi autorisés à jouer au dreidel, qui est une toupie à 4 côtés, chacun
portant une lettre hébraïque.

Les lettres נ ג ה פ se traduisent comme suit: « נס גדול היה פ » – « il y eut un grand miracle ».


.. index::
   pair: Hanouka; Judaïsme en mouvement

.. _hanoukah_jem:

=================================================================================================================================================
**Hanoukah** (inauguration) par Judaïsme en mouvement
=================================================================================================================================================

- https://judaismeenmouvement.org/fetes-juives/la-fete-de-hannoukah/

.. figure:: ../../images/Hanouka.png
   :align: center

   https://judaismeenmouvement.org/fetes-juives/la-fete-de-hannoukah/


Texte
======

**Hanoukah** signifie **inauguration**, celle du Temple.

200 ans avant notre ère, la terre d’Israël a été conquise maintes fois
et beaucoup de Juifs ont été exilés.

Elle est une petite province de l’empire grec dirigé par Antiochus IV Épiphane,
**qui a décidé d’interdire la religion juive**.

Un petit groupe de résistants, les Maccabim, décident de prendre les armes
pour libérer Jérusalem et restaurer le Temple.

Un miracle se produit lorsqu’ils rallument la menora le **25ème jour du mois
de kislev** : les bougies brûlent pendant 8 jours.

C’est en souvenir de ce miracle que les lumières de Hannouka sont allumées
pendant 8 jours.


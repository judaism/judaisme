
.. _hanoukkah_shalom:

=================================================================================================================================================
🕎 The Strange Career of Hanukkah Itself
=================================================================================================================================================

- https://theshalomcenter.org/content/part-ii-strange-career-hanukkah-itself
- https://theshalomcenter.org/hanukkah-strange-career-spirit-and-politics-part-i

Article
============

Submitted by Rabbi Arthur Waskow on 11/30/2021

We have looked at the pre-history of Light in Torah and its appearance
before hanukkah began.

To see that essay on the spirituality of Light, see the lead story on
the `left-hand column of  <https://theshalomcenter.org/hanukkah-strange-career-spirit-and-politics-part-i>`_ (https://theshalomcenter.org/hanukkah-strange-career-spirit-and-politics-part-i).

Now the prehistory becomes an introction. Now let us turn to the Festival of Lights
-- Part II of this essay on the strange career of Hanukkah.

In the 25th of the lunar month Kislev “in the year 145 of the Greek era”
(i.e., 167 BCE), the forces of Antiochus, king of the Hellenistic
successor-state Syria, offered sacrifices on an altar that had been set up
above the Jewish altar to YHWH in the Jerusalem Temple (I Macc.1:59).

This offering climaxed a struggle between Hellenized and anti-Hellenist
Jews, and began a series of bloody attacks on Jewish families that had
circumcised their boy-children, part of a campaign to shatter biblical
Judaism along with all the other indigenous religions of the Eastern
Mediterranean region and replace them with a Hellenistic pattern.

Why did the new sacrifices and the anti-Judaism campaign explode then ?
The Maccabean record does not propose a reason.
It is possible that the date was not accidental but fit into a religious
focus on the darkest time of the year. For we do know that the date itself
in the Jewish lunar moonth comes as the moon is disappearing from public
view, and in the Northern Hemisphere the sun is close to solstice, its
darkest time.

And we do know that in parts of the region from **ancient Israel to Iran**,
in Alexander the Great’s Hellenistic empire, a religion flourished that
celebrated the **sun=god Mithra**, who was said to have been born on the
25th of the solstice month.
So perhaps – we cannot be sure – the 25th of Kiislev had real religious
signicanace to those who dedicated the Temple to  their own sun-god
religion of light reborn in the solstice.

For three years, some Jews who supported biblical Israel's Temple Judaism
waged a furious guerilla war against Antiochus’ Syrian-Greek empire and
its many Jewish sympathizers.

The guerilla bands won, and on the 25th of Kislev **three years later** they
were able to rededicate the Temple to the God of biblical Israel..
According to II Maccabees 10: 1-11 --

They proclaimed **an eight-day festival** to honor Sukkot and Shmini Atzeret,
which they had been unable to observe during the war, with all its joyful
waving of etrog fruit and palm branches.
No mention of an eight-day miracle of olive oil or of lights lit as the
moon and sun darkened and then reawakened.
They decreed that the whole people should observe these eight days every
year, with “hymns to the One who had so triumphantly achieved the
purification of his own Temple.”

But political disaster, the triumph of a new kind of Judaism led by
rabbis, and a continuing popular religious celebration of Light transformed
the eight-day festival.
The Roman Empire –- far stronger than Antioches had ever been – destroyed
the Temple in 70 CE, and shattered the Jewish population in the Land of
Israel **after defeating a rebellion in 135 CE led by Bar Kochba**.

A huge proportion of the Jews were banished and sold as slaves.
The surviving rabbis were unwilling to celebrate the Maccabean rebels
for fear that path would lead to another Bar Kochba disaster.
But they had a solstice-time festival to deal with.
So the Talmudic record of their decisions begins, “Mah zot Hanukkah”  --
What is this Hanukkah?” as if they can barely remember.

While recognizing but limiting celebration of the Macccabean victory,
they set forth a story of a miracle: Olive oil that should have given
light for only one day in the Temple Menorah but lasted for eight days.

God’s miracle, not the Maccabees’ rebellion, justified lighting lamps
for an eight-day festival. And the rabbis made Zechariah’s visions the
prophetic teaching of the Shabbat of Hanukkah, climaxing in “Not by might
and not by power but by My Breathing Spirit,’ says the Infinite Breath of Life.”

And by doing this they also may have brought into Jewish practice a
popular custom of lighting lamps as the dark of moon and sun gave way
to new light as the New Moon rose and the sun grew stronger during the
eight-day festival. A remnant of Mithra, grown Jewish? Perhaps.

Interestingly, a later version of Mithraism flourished in the City of
Rome itself. It may have been because Mithraism and its god-birthday
of December 25 was so popular that in the fourth century of the Christian era,
Christian churches began to adopt the 25th of December as the birthday of Jesus,
destined to become the Son of God.

And so Hanukkah persisted through almost two millennia, until about 1900 CE.
Then three factors disrupted the rabbinic synthesis.

One was the breakdown of many ghetto walls in Western Europe and America.
Those walls had kept Christians with their solar-calendar festival of December 25
separated from Jews with their lunar-solar calendar festival beginning 25 Kislev.
As many ghetto walls dissol Jewish envy of Christmas emerged.
The Christian custom of gift-giving for Christmas was given great power
by the enormous growth of commercial consumerism, and Hanukkah as well
became a beacon for buying.

And the emergence and growth of political Zionism undercut the rabbinic
caution about military solutions to Jewish disempowrment. The Maccabees
became heroes, not questionable guides into possible disaster, and Zionist
songs changed references to the spiritual  power of God to the
politico-military power of the people armed. (For Rabbi David Seidenberg’s
comments on the songs, see http://www.neohasid.org/resources/mi_yimalel/

Now we face the possibility of another great change in Hanukkah.
Jewish youth alongside the youth of other communities are increasingly
worried about the threat to their future through global scorching and
the climate crisis.
They are increasingly critical of the whole machinery of consumer
commercialism and dependence on burning fossil fuels.

Many are critical of what they see as Israeli militarism.
**So a different aspect of Hanukkah comes forward**.

That aspect is seeing the legend of the eight days’ usefulness of one
day’s olive oil as a call not to wait for God’s miracle but to take human
action to conserve energy and turn to new sources of renewable energy
to heal God’s Earth, not destroy it.

Some also see Zechariah’s vision of the tiny cyber-forest of olive trees
feeding oil directly into the tree-shaped Light-bearing Menorah as a call
to covenant between sacred human action and other sacred life-forms.

So perhaps Hanukkah begins to become a new practice for uniting physical
activism, emotional empathy, ecological intellect, and awe-inspired Spirit
–- the Four Worlds –- into One Breath that unites all life.


The eighth candle of Hannukah completes the sacred lighting, but it is more than a completion. The number 7 is fulfillment; the number 8 is Beyond. Infinity
================================================================================================================================================================

- https://mailchi.mp/theshalomcenter/hanukkah-at-the-roots-night-8-infinity-and-love?e=80da458242

The eighth candle of Hannukah completes the sacred lighting, but it is more than
a completion. The number 7 is fulfillment; the number 8 is Beyond. Infinity. (The
mathematicians’ symbol for Infinity is the number 8, tilted to lie on its
side. ∞ )

What does this teach us? Perhaps the most uncanny, “beyond,” one might
say “infinite” story of Hanukkah is in and indeed beyond the Prophet
Zechariah’s haftarah for Shabbat Hanukkah. Zechariah has a conversation with
a Messenger from YHWH, the Interbreathing Spirit of the world. The Messenger
shows him a secret Truth, then asks him whether he knows what it means. Zechariah
keeps saying “No!” and the Messenger explains.

One of these images is that when the Second Temple is built, the Great Menorah,
modeled on a Tree with branches, buds, bunches of green leaves unfolding every
bud of Light, will be accompanied by an olive tree to its left and another
to its right. The Messenger asks, “Do you know what this means?” and the
Prophet says “No!” The Messenger says, “Not by might, and not by power,
but by My [Interbreathing] Spirit says YHWH, Breath of Life.”

Already this is mysterious, uncanny. And beyond the haftarah is another image:
The two olive trees are pouring golden oil straight into the golden Tree of
Light. Human beings have crafted one of these trees; the epochs-long process
of Evolution has crafted the other two. Together they bespeak the emergence
of Holy Light and their relationship is Beyond the Telling and Beyond
explaining. Infinite.

Our Hanukkah must reach beyond the many stories of its origins. Even beyond
the many issues we have found in the stories — the agony of Israel and
Palestine, climate, democracy, poverty, cruelty toward Trans people, racism,
abortion rights.

We must act on these issues and reach beneath them and beyond them to the
spiritual failings and the spiritual transformations that are encoded in them
and beyond them.

Our failings stem mostly from Greed — Greed for more land, more possessions,
more prestige, more wealth, more power. Our inner sense of transformation and
the eco-social transformations we call peace and truth and justice stem from
Love. Love, the Song of Songs tells us, is strong as death. A truth that is
only true if we make it so.

If we do, we embody the Infinite, the Light that suffuses all the worlds.

— by Rabbi Arthur Waskow

.. index::
   pair: Hanouka; Floriane Chinsky

.. _hanouka_floriane_chinsky:

=================================================================================================================================================
**Le premier sens de Hanouka: la fête de la lumière universelle**
=================================================================================================================================================


La fête de la lumière universelle (12 novembre 2015)
======================================================

- https://libertejuive.wordpress.com/2015/11/12/hanouka-universel/

En ce jour de `Roch Hodech Kislev <https://www.hebcal.com/holidays/rosh-chodesh-kislev-2015>`_ (https://www.hebcal.com/holidays/rosh-chodesh-kislev-2015),
voici un petit article sur Hanouka sorti de mes tiroirs et toujours d’actualité… חודש טוב לכלם

Hanouka, entre toutes les fêtes, illustre l’attachement du peuple juif
à sa tradition, une tradition de vie, porteuse de vie, adaptée à nos vies.

Absente de la Bible, à peine évoquée dans la michna, Hanouka nous apparaît
pour la première fois dans le Talmud.

Une braïta (source tanaïtique), citée dans le traité chabat du Talmud
babylonien (21b) présente le lien entre la célébration et l’allumage de bougies.

Hanouka est, selon le talmud lui-même, originé dans un phénomène naturel
impressionnant : **le raccourcissement des jours**.
Il est essentiel de se mettre dans la peau des générations précédentes.
L’amoindrissement de la luminosité et du temps d’exposition au soleil
nous influence tous, parfois de façon imperceptible ou subliminale.

Il nous est facile aujourd’hui, pour peu que nous en prenions conscience,
d’allumer une lampe ou une bougie.
La situation était toute autre dans le passé.
L’absence de l’électricité rendait l’éclairage très difficile, très
fragile et très peu confortable.

**La lumière vacillante des bougies elle-même constituait un luxe**.
Le Talmud va jusqu’à poser la question du choix à opérer si on ne dispose
pas de l’argent nécessaire pour se procurer à la fois les bougies de
Hanouka et le vin du kidouch du chabat !

Lorsque petit à petit, dans notre univers physique, dans notre univers
familial, dans notre univers personnel, la lumière diminue, il est
important d’en prendre conscience. C’est le premier message de Hanouka.

Cette prise de conscience, pourtant, peut faire peur. Le talmud fait
remonter cette première angoisse à **Adam Harichon**, le premier humain.
Voyant les jours raccourcir, torturé de culpabilité face à la faute qui
l’avait chassé du jardin d’Eden, Adam vit sa fin venir. Il s’imagina
disparaître dans l’obscurité, poursuivi par la faute de la consommation
du fruit de l’arbre de la connaissance.
Il entreprit alors, nous dit le Talmud, de jeûner et de prier.

Quand vint l’époque du mois de tévèt, il observa le rallongement des jours,
il se dit qu’il s’agissait du fonctionnement normal du monde, et se
consacré à huit jours de fête (avoda zara 8a).
C’est avec la renaissance des jours qu’Adam pu reprendre sa vie.
C’est ainsi que naquit la fête.
La première origine de la fête serait liée à la nature, et à son influence
sur nos sentiments et comportement.

Allumer les lumières de Hanouka permet de nous situer en harmonie avec
la nature, sans nous laisser dominer par elle, de ne pas laisser nos
jours se raccourcir et notre univers se réduire comme une peau de chagrin.

Au contraire, connaître la nature nous permet d’anticiper son cours et
de nous prémunir de ses atteintes, de nous nourrir de ses offrandes.

Dans ce premier sens, Hanouka est une fête universelle, c’est la fête
des lumières, accompagnée par les guirlandes laïques et chrétiennes qui
accompagnent elles-aussi l’obscurité de l’hiver….

Hanouka a également un sens spécifique à l’identité juive, nous l’évoquerons
dans les prochains jours.


Hanouka: un message d’espoir historique et légendaire (26 novembre 2015)
==============================================================================

- https://libertejuive.wordpress.com/2015/11/26/hanouka-historique/

Hanouka a également une raison historique, souvent ignorée.

Les livres des Machabées, livre apocryphe qui ne nous est parvenu qu’à
travers la traduction des septante retrace l’épopée des makabim, de
Matatiahou et de ses fils, leaders de la révolte.

Il y est relaté qu’une fois le temple reconquis et remis en état, chacun
a regretté de n’avoir pu y célébrer la fête de soukot.

Le deuxième livre des makabim raconte :

    « Ils ont célébré dans la joie, huit jours comme la fête de soukot,
    se souvenant que quelques temps auparavant ils avaient passé les
    jours de soukot dans les montagnes et les cavernes comme des animaux.
    Pour cette raison ils ont décoré avec des bâtons et des branches
    qu’on pouvait trouver à cette saison et avec des palmiers dattiers
    ils ont loué Dieu qui avait soutenu leur action dans la purification
    de son sanctuaire. » (makabim II, 10 :6-7)

**La deuxième origine de la fête est donc historique.**

La situation de Sion comme terre conquise et les conditions de la révolte
avaient empêché la célébration de soukot.
Notre peuple, qui ne renonce jamais, a su recréer cette fête avec les
moyens du bord, hors saison.
Notons au passage que ce « soukot chéni » n’a pas pu obéir aux règles
habituelles de la fête. Cela n’a pas empêché le peuple de la célébrer
et de témoigner ainsi son amour pour la tradition.

C’est une deuxième leçon de la fête : **ne jamais renoncer**, ne pas se
laisser démoraliser par l’impossibilité de célébrer nos fêtes, tenter
de « reconquérir le sanctuaire » en réunissant les conditions qui nous
permettrons de poursuivre notre tradition, et, lorsque le sanctuaire
est reconquis, ne pas l’ériger en vérité pétrifiée et idolâtre, mais
témoigner de notre amour pour la tradition de façon créative.


Le miracle de la fiole d’huile
===================================

- https://libertejuive.wordpress.com/2015/11/26/hanouka-historique/


Hanouka, et cette partie de l’histoire est la plus connue, possède
également un fondement légendaire, psychologique et symbolique.
Il s’agit du miracle de la fiole d’huile, qui a permis à la ménora
(lampe à sept branches utilisée au temple) de brûler pendant huit jours
avec une toute petite quantité d’huile.

Ainsi, une sorte de ménora à huit branches (comme les huit jours de hanouka)
plus chamach, que l’on nomme **Hanoukia**, a fait son apparition au
quatrième siècle.

**Encore une fois, il s’agit de ne pas se laisser décourager par la
petitesse de nos moyens**.

Si nous n’avons qu’un peu d’huile, utilisons-là, partageons cette lumière,
laissons là nous éclairer, et elle ne viendra pas à s’éteindre.

Pour cette année, nous pouvons retenir de cette fête si belle et si
simple trois qualités qui nous accompagneront pendant ces huit jours,
et espérons-le au delà :

- la lucidité et la conscience,
- la créativité  dans l’expression de notre amour,
- l’espoir.






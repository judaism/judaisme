.. index::
   pair: Vocabulaire; Hanouka


.. _hanouka_voca:

=================================================================================================================================================
Hanouka vocabulary 🕯️🕎🔥
=================================================================================================================================================

- https://www.myjewishlearning.com/article/how-to-greet-someone-on-hanukkah/
- https://www.myjewishlearning.com/article/hanukkah-vocabulary/

Introduction
===============

The winter holiday of **Hanukkah** commemorates the victory of the Maccabees
and the subsequent **rededication of the Temple in Jerusalem**.

The eight-day festival is a joyous occasion, so greeting Jewish friends,
family and colleagues with warm **Hanukkah** wishes will surely be welcomed.

Due to its proximity to Christmas, **Hanukkah** has become one of the most
well-known Jewish holidays, despite being a minor holiday that isn’t
even mentioned in the Torah.

While “Happy Holidays” isn’t inappropriate to say during the Festival of
Lights, there are several Hanukkah-specific phrases you can use to greet
someone on Hanukkah.

In English, it’s perfectly normal to say “Happy Hanukkah.”
---------------------------------------------------------------

- https://fediverse.org/AbbyChavaStein/status/1465048179288727556?s=20


To say Happy Hanukkah in Hebrew, you can say **chag chanuka sameach**,
(pronounced chahg cha-nu-KAH sah-MAY-ach.)

In Israel, it is common to just say chanuka sameach.


chag urim sameach! 🕯 to all my twitter chevrei, I love you so much
--------------------------------------------------------------------

Some Israelis prefer chag urim sameach (pronounced chahg oo-REEM sah-MAY-ach),
which literally translates to “Happy Festival of Lights.”

- https://fediverse.org/sarahdayarts/status/1465109486830374913?s=20


https://emojis.wiki/fr/hanoucca/
===================================

- https://emojis.wiki/fr/hanoucca/



- 🥔🍳🥞 (Crêpes aux pommes de terre)
- 🍫🍩🍓😋 (beignets fourrés)
- 🍩 beignet
- 😊 Je vous souhaite une fête remplie de 😄 joie et de 🤗 moments heureux. Joyeux Hanoucca!
- Il est temps d'allumer une 🕯 bougie
- Avez-vous allumé des 🕯 bougies sur la 🕎 Menorah?
- 🙏 Bonnes fêtes de fin d'année! 🕎 Joyeuse Hanoucca!


Stop and look on how many ways we spell #Chanukah/#Hanukkah/#Hanikkeh, etc.
================================================================================

- https://fediverse.org/AbbyChavaStein/status/1464249824400785422?s=20
- https://fediverse.org/Shmarya/status/1464325609228701697?s=20

Stop and look on how many ways we spell #Chanukah/#Hanukkah/#Hanikkeh, etc.

We can't even agree on the name of one our most famous holidays.
We don't agree on much. And that's beautiful!


Beignet 🍩
=============

- https://emojis.wiki/fr/hanoucca/

- 🍫🍩🍓😋 (beignets fourrés)
- 🍩 beignet

- https://fediverse.org/noemie_issan/status/1207713146275794944?s=20

Sauf qu'une fois deshabillés, une fois leurs perruques de crème ôtées,
leur glacage trompe-couillons degusté, ils ne peuvent plus cacher ce
qu'ils sont irremissiblement.
Des putains de beignets à l'huile. Nus, tous les beignets sont égaux.


Ceci est une métaphore de l'eternité de Hanouka. Un être simple confronté
a la sophistication et a la volonté du transfuge de classe.
Assumons notre être beignet a la face du monde. Sans compromis.


- https://fediverse.org/CoolZionist/status/1464874992211013632?s=20

Cette femme est folle. Réussir à philosopher sur des beignets, j'étais
pas prêt. Hanoukka sameah !!



- Hag Hanukkah Sameah


Spellings of חנוכה listed by the OED
=====================================

:source: https://fediverse.org/RaananEichler/status/1339292034365054976?s=20

::

    Chanucha
    Chanuchah
    Hanuca
    Hanucka
    Chanuca
    Chanucah
    Chanucca
    Chanuccah
    Chanuka
    Chanukah
    Chanukka
    Chanukkah
    Hanucah
    Hanucca
    Hanuccah
    Hanucha
    Hanuckah
    Hanuka
    Hanukah
    Hanukka
    Hanukkah
    Khanukah
    Khanukka
    Khanukkah


- https://fediverse.org/Lizaficati0n/status/1465038289333854213?s=20

::

    Happy Chanukah Hanukka Hanukkah Chanukka Chanuca Khanukkah Hanuccah Hanukah


.. index::
   pair: shomer; shabbat
   ! shabbat
   ! shomer shabbat

.. _shabbat:

=======================================================================================================
**shabbat, שבת, shabbes, שאַבעס**
=======================================================================================================

- https://fr.wikipedia.org/wiki/Shabbat

.. figure:: images/Shabbat_Candles.webp
   :align: center

   La table typique du shabbat : une coupe de kiddoush, deux hallot (recouvertes d'un napperon) et deux bougies.


Définition
==============

Le shabbat ou chabbat (hébreu : שבת, « cessation, abstention » ; en
yiddish שבת ou, rarement, שאַבעס, shabbes) est le jour de repos assigné
au septième jour de la semaine Biblique, le samedi, qui commence dès la
tombée de la nuit du vendredi soir.

Le shabbat est officiellement jour chômé en Israël, et outre les magasins,
les transports publics ne fonctionnent pas.

Élément fondamental des religions Israélites, il est observé par beaucoup
de fidèles.

Au-delà des notions de « prescrit » et de « proscrit » (ou, selon une
interprétation plus littérale, de « permis » et d'« interdit »),
le shabbat est surtout considéré **comme un jour hors du temps et des
contingences matérielles**, un jour durant lequel toutes les activités
extérieures doivent être réduites pour se concentrer sur la famille
et le foyer.

Il y est surtout question d'activités dans son cercle familial, de moments
pour se ressourcer, de repas en famille…

Il commence le vendredi, 18 minutes avant le coucher du soleil et se
termine le samedi après l'apparition de 3 étoiles moyennes (approximativement
40 minutes après le coucher du soleil), soit une durée variant entre
25 heures et 25 h 30 min selon les saisons.

La période supplémentaire (Tosefet shabbat) avant le coucher du soleil
n'est pas partout de 18 minutes, car pour certains elle est de 22 ou 24 minutes,
à Safed de 30 minutes, à Jérusalem de 40 minutes.

L'observance des heures est très précise chez les haredim et il existe
des « tableaux des heures dites » (lou'hot) distribués dans les communautés.

Maale Adumim et Petach Tikvah observent la même heure que Jérusalem car
leurs premiers habitants venaient de Jérusalem et ils y ont apporté leurs traditions.

- Movaé shabbat (מובאי שבת) : entrée du shabbat.
- Motsaé shabbat (מוצאי שבת) : sortie du shabbat.


Observance
============

Le chabbat est un jour de célébration autant que de prière.
Trois repas meilleurs que l'ordinaire, les shalosh seoudot, sont offerts
à la fin de chaque office :

- Seouda rishona le Erev shabbat, le vendredi soir
- Seouda shenit après shaharit + Moussaf, le samedi, un peu après midi
- Seouda shlishit, entre Minha et Arvit de Motsei shabbat (la prière du
  soir qui clôture chabbat), en fin d'après-midi les meilleures denrées
  sont réservées pour shabbat.

  Dans certains milieux moins favorisés, c'est le seul jour où l'on mange
  de la viande, bien qu'il n'y ait pas de stricte obligation à en consommer,
  comme ce serait le cas à Yom tov.

Melave Malka représente le dernier repas à la sortie de shabbat destiné
à retarder le départ de ce jour saint : seuls les plus pratiquants ajoutent
ce dernier repas.

L'affluence des fidèles est également plus importante le shabbat qu'en
semaine (même si elle n'égale pas Yom Kippour). Elle est pour certains
le seul jour de pratique religieuse.

Le shabbat étant **un jour de fête et de réjouissance**, tout jeûne y est proscrit.

Tout autre jeûne que celui de Yom Kippour (qui n'est pas un jour de deuil
malgré les signes extérieurs de mortification) tombant un shabbat doit
être reporté au jeudi précédent, en règle générale.

De même, les endeuillés en période de chiv'ah doivent se conduire « normalement »
le chabbat, sans exprimer ouvertement les signes extérieurs de deuil
(comme la Qeri'ah).
Ils restent toutefois astreints au deuil en privé, ne se lavant pas et
se réfrénant de toute activité joyeuse ou sexuelle.

Celui qui observe le shabbat, en respectant les prohibitions et en pratiquant
les prescriptions positives est appelé **shomer shabbat (en) ou gardien du shabbat**.



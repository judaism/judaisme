.. index::
   ! glossaire

.. _judaisme_glossaire:

=====================================================================================================
Glossaire
=====================================================================================================


.. glossary::

   Ashkénazes
       Juifs issus de la communauté juive qui étaient autour du Rhin au
       moment des croisades et a été dispersés par les croisades.

       Ce sont les juifs principalement compris entre le Rhin et l'Oural
       (mais pas les juifs d'Italie qui sont les Italkims, ni les juifs
       des Balkans et de Grèce qui sont romaniotes et sepharades au sens
       strict)

   Michna
       Source: "Des femmes et des dieux".
       Première strate de la rédaction, au IIe siècle, d'éléments de la
       Torah orale. La Michna comprend des opinions de rabbins sur les
       pratiques à suivre, l'exposé de déaccords entre eux, des histoires
       et des versets de la Torah écrite utilisés pour faire avancer les
       débats.

   Kavod

       - https://www.france.tv/france-2/a-l-origine/5320821-horizon-le-kavod.html

       Le Kavod est un principe moral et spirituel du judaïsme, qui
       évoque le respect.

       Il est très important dans cette religion.

       Nous en parlons aujourd'hui avec le Rabbin Michaël Azoulay.

   Maghrebi
       Juifs originaires du Maghreb avant les migrations des juifs sépharades.
       Souvent de culture judeo-amazigh (exemple Gad Elmaleh né au Maroc
       dont une partie de la famille est Juive ET Chleuh) , ou de culture
       judeo-arabe (souvent donc arabisés comme la population majoritaire
       au Maghreb) (exemple : :ref:`Hannah Assouline <guerrieres:hanna_assouline>`)

   Sepharades
       Au sens strict :  juifs issus de la dispersion des juifs de péninsule
       ibérique, ils se sont globalement éparpillés à Amsterdam, au Sud
       de l'Italie ( Livourne) avant de migrer vers l'Afrique du Nord,
       en Afrique du nord, en Turquie , en Grèce , au territoire  qui est
       devenu la Palestine, et même un peu dans le Sud de la France qui
       n'était pas français à l'époque ...
       Bref partout sur les bords de la Méditerranée.
       Et  dans les colonies espagnoles naissantes aux Amériques (en se
       cachant parce que c'était espagnol et qu'ils avaient pas le droit
       d'être là, mais du coup les plus vieilles communauté aux Caraïbes
       ça vient d'eux, et ils se sont pas mal mixés avec d'autres populations)
       Et même en partie en Inde (https://fr.wikipedia.org/wiki/Synagogue_Paradesi )
       (Exemple : Enrico Macias , chanteur juif franco-algérien qui chante
       je suis un juif espagnol et fait de la musique aux racines judéo-andalouses)

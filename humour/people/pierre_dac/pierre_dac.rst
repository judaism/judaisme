.. index::
   pair: Humour; Pierre DAC
   pair: Humour; André Isaac

.. _pierre_dac:

=======================================================================================================
André Isaac, dit **Pierre Dac**
=======================================================================================================

- https://fr.wikipedia.org/wiki/Pierre_Dac



.. figure:: Dac_Harcourt_1947.jpg
   :align: center

   Par Studio Harcourt — RMN, Domaine public, https://commons.wikimedia.org/w/index.php?curid=76270219


Présentation
===============

André Isaac, dit Pierre Dac, officiellement André Pierre-Dac à partir
de 1950, né le 15 août 1893 à Châlons-sur-Marnenote et mort le 9 février 1975
dans le 17e arrondissement de Paris, est un humoriste et comédien français.

Il a également été, pendant la Seconde Guerre mondiale, une **figure de la
Résistance contre l'occupation de la France par l'Allemagne nazie grâce
à ses interventions sur Radio Londres**.

Créateur dans les années 1930 du journal humoristique L'Os à moelle,
Pierre Dac est notamment l'inventeur du **Schmilblick**, un objet
*rigoureusement intégral, qui ne sert absolument à rien et peut donc servir à tout*.

Il popularise également l'expression **loufoque**, formée à la façon du
louchébem. Il est également inventeur du biglotron, et de la célébrissime
recette de la confiture de nouilles.



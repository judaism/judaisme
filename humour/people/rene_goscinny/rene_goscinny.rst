
.. index::
   ! René Goscinny

.. _rene_goscinny:

=======================================================================================================
René Goscinny
=======================================================================================================

- https://fr.wikipedia.org/wiki/Ren%C3%A9_Goscinny

Biographie
===========
- https://fr.wikipedia.org/wiki/Ren%C3%A9_Goscinny


René Goscinny, né le 14 août 1926 à Paris où il est mort le 5 novembre 1977,
est un scénariste de bande dessinée, journaliste, écrivain et humoriste
français, également producteur, réalisateur et scénariste de films.

Il est l'un des fondateurs et rédacteurs en chef de Pilote, l'un des
principaux journaux français de bande dessinée.

Créateur d'Astérix avec Albert Uderzo, d'Iznogoud avec Jean Tabary, auteur
du Petit Nicolas, personnage créé et dessiné par Jean-Jacques Sempé,
scénariste de nombreux albums de Lucky Luke créé par Morris.

Il est l’un des auteurs français les plus lus au monde : l’ensemble de
son œuvre représente environ 500 millions d’ouvrages vendus.

Avec Jean-Michel Charlier, il joue un rôle décisif pour la reconnaissance
du métier de scénariste de bande dessinée qui n’existait pas avant lui.


Origines et jeunesse
=========================

René Goscinny est né le 14 août 1926 au 42 rue du Fer-à-Moulin dans le
5e arrondissement de Paris.

Cette naissance la veille d'un jour férié lui fera dire qu'il est un « paresseux contrarié ».
Il est issu d'une famille juive ashkénaze, originaire de Pologne et
d'Ukraine (gościnny signifie « hospitalier » en polonais).

Son père Stanislas, dont le prénom hébraïque est Simha5, est un ingénieur
chimiste né le 27 septembre 1887 à Varsovie (Pologne alors russe),
troisième fils du rabbin Abraham Gościnny et Helena Silberblick.

Stanislas s'installe à Paris en 1906, au moment de l'inflation des
persécutions contre les Juifs en Europe centrale.

Sa mère, Anna née Bereśniak, née le 7 mai 1889 à Chodorów alors en
Galicie austro-hongroise (aujourd'hui Khodoriv en Ukraine, oblast de Lviv),
est issue d'une famille juive non-pratiquante bien que cultivant la langue
hébraïque et l'éducation aschkénaze par l'étude des lettres et de la musique,
investie dans l'édition.

Victime de pogroms9,N 1, Lazare Abraham Beresniak (-1942), le grand-père
maternel de René Goscinny s'établit avec son épouse, Freiga Garbel, à Paris
en 1905 ou 1912, au 12 de la rue Lagrange, où il tient une imprimerie à son nom.

**À l'époque, l'imprimerie Beresniak s'occupe notamment de l'édition de
plusieurs des principaux journaux yiddishophones et russophones en cyrillique
de Paris**.

L'entreprise est plus tard reprise par les fils Beresniak qui emploieront
une centaine de personnes dans les années 1930. Abraham rédige le premier
et seul dictionnaire existant hébreu-yiddisch, publié en France en 1939.

Certains des membres de la famille Beresniak choisissent de partir à
l'étranger à temps (en Argentine et aux États-Unis) alors que l'imprimerie
parisienne **fait imprimer de faux papiers pour le reste de la famille et
son entourage**, ce qui permet de gagner la province pour s'y cacher.

Sous l'Occupation, l'imprimerie Beresniak n'échappe pas à la spoliation
des biens juifs par les Nazis.

À la Libération, le seul oncle rescapé de René Goscinny, Serge Beresniak,
récupère l'entreprise qui s'installe en 1959, au 18-20 de la rue du
Faubourg du Temple, et imprimera en 1973, la version première russe de
L'Archipel du goulag de Soljenitsyne.

Stanislas Goscinny et Anna Beresniak se rencontrent à Paris et se marient
en 1919. Ils sont naturalisés français en août 1926, quelques jours avant
la naissance de René.

René Goscinny a un frère aîné, Claude ; il est le cousin germain du
philosophe Daniel Béresniak et apparenté au médecin suisse Ariel Beresniak.


Enfance en Argentine
========================

Dès 1923, le père de René Goscinny tente une expérience dans la production
agricole au Nicaragua.

En 1927, Stanislas Goscinny est employé par la Jewish Colonization Association (JCA),
destinée à aider et favoriser l'émigration des Juifs d'Europe ou d'Asie,
notamment sur le continent américain, afin d'échapper notamment aux statuts
iniques de l'Empire russe.

C'est à ce titre que les Goscinny partent pour Buenos Aires, en Argentine,
où Stanislas est employé comme ingénieur chimiste. Une partie de la famille
l'y rejoint en 1928. Ces terres accueillent un monde d'immigrés juifs,
marquées par « un grand activisme culturel et politique », terreau fertile
pour attiser la curiosité, la culture, l'esprit pionnier et la créativité
du futur père d'Astérix, qui baigne alors dans le cosmopolitisme et le
multilinguisme où l'on parle yiddish, français et espagnol.

Sculptures représentant les personnages Patoruzito et Isidorito, à San Telmo,
Buenos Aires, (Argentine)

René Goscinny étudie au lycée français de Buenos Aires où sa culture se
nourrit du pur classicisme de la tradition française.
Il passe ses grandes vacances en Uruguay où il monte à cheval dans la
pampa avec les gauchos. Il a l’habitude de faire rire ses camarades de
classe, probablement pour compenser une timidité naturelle.

Il commence à dessiner très tôt, inspiré par les histoires illustrées des
aventures de Patoruzù (es), un Indien patagon, héros populaire des historietas
argentines créé par le dessinateur Dante Quinterno, et également celles
comme Zig et Puce, Superman, Tarzan et surtout Les Pieds Nickelés dont il
recopie scrupuleusement l'album qu'il ramène de Paris.

À cette époque, la famille Goscinny s'embarque fréquemment sur des
transatlantiques, pour visiter les grands-parents et cousins de France ;
pour René Goscinny, la France « était le pays fabuleux, exotique, où nous
partions en vacances. Nanterre, les Deux-Sèvres, c'était Tombouctou.

Il se passionne très tôt pour le cinéma, son acteur préféré étant Stan Laurel.


Durant la Seconde guerre mondiale
===================================

En Europe, la Seconde Guerre mondiale commence.

Si sa famille directe est à l’abri en Argentine, une partie de celle restée
en Europe est victime de la Shoah. Des lettres de sa famille restée en
France sous l’Occupation racontent les brimades quotidiennes, le port de
l’étoile jaune….

Trois de ses oncles maternels, arrêtés après une dénonciation pour avoir
imprimé des tracts antiallemands, meurent en déportation dans les camps
de Pithiviers et d’Auschwitz.
La famille Goscinny ne découvre cela qu’à la Libération, ce qui traumatisera
durablement le futur scénariste.

De l’autre côté de l’Atlantique, Stanislas rejoint le « Comité de Gaulle »
dès août 1940. Mais le 26 décembre 1943, peu après l’obtention de son bac
à dix-sept ans, le jeune René Goscinny perd son père, des suites d’une
hémorragie cérébrale, ce qui fait basculer la famille dans la précarité.

Il se voit obligé de rechercher un travail, et est notamment dessinateur
dans une agence de publicité. Parallèlement, il publie ses premiers textes
et dessins dans Quartier latin et Notre Voix, bulletin interne du collège
français de Buenos Aires.


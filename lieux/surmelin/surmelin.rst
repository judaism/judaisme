.. index::
   pair: Lieux; Surmelin
   ! Surmelin

.. _surmelin:

=====================================================================================================
**Surmelin** Rue du Surmelin, Paris 20e, 48.868148 LN , 2.403686 E
=====================================================================================================

- https://judaismeenmouvement.org/lieux/surmelin/
- https://www.instagram.com/nelech_surmelin/ (Groupe d’étudiant.e.s partageant 
  les enseignements de @floriane_chinsky en chemin vers un judaïsme égalitaire et inclusif 🍃🌱🌿🪴) 
- https://what3words.com/brider.racontons.verbe
- https://www.geoportail.gouv.fr/?c=2.4037587607032336,48.868125592384274&z=19&l0=GEOGRAPHICALGRIDSYSTEMS.MAPS.3D::GEOPORTAIL:OGC:WMTS==aggregate(1)&l1=LIMITES_ADMINISTRATIVES_EXPRESS.LATEST::GEOPORTAIL:OGC:WMTS(1)&l2=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2::GEOPORTAIL:OGC:WMTS(1)&permalink=yes

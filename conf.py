# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
# sys.path.insert(0, os.path.abspath("./src"))

project = "Judaisme"
html_title = project

author = "Noam"
html_logo = "images/th.jpeg"
html_favicon = "images/th.jpeg"
release = "0.1.0"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
today = version

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
    "sphinx_copybutton",
]
extensions += ["sphinx.ext.intersphinx"]


# https://sphinxcontrib-youtube.readthedocs.io/en/latest/usage.html#configuration
extensions.append("sphinxcontrib.youtube")


# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", ".venv"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions.append("sphinx.ext.intersphinx")
intersphinx_mapping = {
    "floriane": ("https://judaism.gitlab.io/floriane_chinsky/", None),
    "bokertov": ("https://judaism.gitlab.io/bokertov/", None),
    "judaisme_2023": ("https://judaism.gitlab.io/judaisme-2023/", None),
    "link_judaisme": ("https://judaism.gitlab.io/linkertree/", None),
    "guerrieres": ("https://luttes.frama.io/pour/la-paix/les-guerrieres-de-la-paix/", None),
    "jjr": ("https://jjr.frama.io/juivesetjuifsrevolutionnaires/", None),
}
extensions.append("sphinx.ext.todo")
todo_include_todos = True

# material theme options (see theme.conf for more information)
# https://github.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

# material theme options (see theme.conf for more information)
html_theme_options = {
    "base_url": "https://judaism.gitlab.io/judaisme",
    "repo_url": "https://gitlab.com/judaism/judaisme",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "indigo",
    "color_accent": "cyan",
    "theme_color": "#2196f3",
    "master_doc": False,
    "nav_title": f"{project} ({today})",
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "https://judaism.gitlab.io/floriane_chinsky/",
            "internal": False,
            "title": "Floriane Chinsky",
        },
        {
            "href": "https://fr.wikipedia.org/wiki/Portail:Juda%C3%AFsme",
            "internal": False,
            "title": "Judaisme",
        },
        {
            "href": "https://judaism.gitlab.io/judaisme-2023",
            "internal": False,
            "title": "Judaisme 2023",
        },
        {
            "href": "https://judaism.gitlab.io/judaisme-2024",
            "internal": False,
            "title": "Judaisme 2024",
        },
        {
            "href": "https://judaism.gitlab.io/linkertree/",
            "internal": False,
            "title": "Liens Judaisme",
        },
        {
            "href": "https://akadem.org/",
            "internal": False,
            "title": "Akadem",
        },
        {
            "href": "https://akadem.org/public/NL/Tempo/Scope-Jour.html",
            "internal": False,
            "title": "Akadem Scope",
        },
        {
            "href": "https://www.youtube.com/@AkademTV/videos",
            "internal": False,
            "title": "Akadem Videos",
        },
        {
            "href": "https://www.mahj.org",
            "internal": False,
            "title": "Mahj",
        },
        {
            "href": "https://www.iemj.org/",
            "internal": False,
            "title": "IEMJ",
        },
        {
            "href": "https://www.franceculture.fr/emissions/talmudiques",
            "internal": False,
            "title": "Talmudiques",
        },
        {
            "href": "https://www.centre-medem.org",
            "internal": False,
            "title": "Centre medem",
        },
        {
            "href": "https://www.france.tv/france-2/a-l-origine/toutes-les-videos/",
            "internal": False,
            "title": "A l'origine",
        },
        {
            "href": "http://www.calj.net/",
            "internal": False,
            "title": "Calj",
        },
        {
            "href": "https://www.hebcal.com/",
            "internal": False,
            "title": "Hebcal",
        },
        {
            "href": "https://linkertree.frama.io/pvergain/",
            "internal": False,
            "title": "Liens pvergain",
        },
    ],
    "heroes": {
        "index": "Judaisme",
    },
    "table_classes": ["plain"],
}
# https://github.com/sphinx-contrib/yasfb
extensions.append("yasfb")
feed_base_url = html_theme_options["base_url"]
feed_author = "Scribe"
# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")
# https://sphinx-tags.readthedocs.io/en/latest/quickstart.html#installation
extensions.append("sphinx_tags")
tags_create_tags = True
# Whether to display tags using sphinx-design badges.
tags_create_badges = True

language = "en"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True


copyright = f"2020-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"


rst_prolog = """
.. |sefaria| image:: /images/sefaria_avatar.jpg
.. |FluxWeb| image:: /images/rss_avatar.webp
"""
